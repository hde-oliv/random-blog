---
title: "000 - Hugo"
date: 2021-07-23T11:49:18-03:00
image: "hugo-logo-wide.svg"
draft: false
---

Eu sempre tive vontade de fazer algum site ou algo do tipo para escrever sobre alguma coisa, só para ter o hábito de redigir tanto em português quanto em inglês (em breve) ou quem sabe um dia em Alemão (who knows?).
E ontem eu estava com uma preguiça imensa de continuar o meu curso de Java ('cause it's Java) e resolvi dar uma olhada no GitHub Pages.

https://pages.github.com/

É bem comum encontrar algumas coisas por aí usando o GitHub pages como host, por exemplo, esta calculadora de fusões para SMT e outros jogos da franquia.

https://aqiu384.github.io/megaten-fusion-tool/home

E então juntei lé com cré e resolvi criar o meu e usar o primeiro post para listar o que tive que fazer (parece fácil mas tem uns detalhes chatos).

A seguir o passo a passo para quem se interessar e em outro post escrevo algo mais "cool":

## Criar um repositório no GitHub

Como vamos usar o GitHubPages, teremos que criar um repositório especial seguindo basicamente o passo a passo do [site](https://pages.github.com) deles.
    
1. Primeiro criamos um repositório chamado: <_login_>.github.io

2. E seguindo o tutorial pelo terminal (se você não tem familiaridade com o terminal, recomendo comece a usá-lo mais, um dia você vai precisar) temos que cloná-lo na nossa máquina

```shell
git clone https://github.com/<login>/<login>.github.io
```

Os próximos passos são apenas para ver algo no ar, depois apagaremos o que foi feito.

3. Agora entramos na pasta do repositório com `cd` e damos `echo` num arquivo (tecnicamente não é isso, mas whatever) chamado index.html:

```shell
cd <login>.github.io
echo "Hello World" > index.html
```

4. Damos um commit com tudo:

```shell
git add --all
git commit -m "Initial commit"
git push -u origin main
```

E voilá, já está no ar.


## Seguir os passos básicos do Hugo

Como o site padrão é bem feio, e eu não queria perder muito tempo o construindo do zero, resolvi usar um framework chamado Hugo.

https://gohugo.io/

Agora vamos seguir o [quickstart](https://gohugo.io/getting-started/quick-start/) deles:

1. Primeiro ele precisa ser instalado na sua máquina (é bem leve):

```shell
brew install hugo  # MacOs
choco install hugo -confirm  # Windows
snap install hugo  # Linux
```
Você pode instalá-lo diretamente baixando-o [aqui](https://github.com/gohugoio/hugo/releases). Vamos precisar da versão extended para o Live Server.

2. Antes de tudo, se você fez os passos 3 e 4 na seção do GitHubPages, você vai precisar deletar o site antigo da seguinte forma:

   Apagando tudo o que há na pasta do repositório e dando commit nesta alteração!
```shell
rm -rf * 
git add *
git commit -m "Delete old site"
git push -u origin main
```

3. Usamos o programa do Hugo para criar nosso site:

```shell
hugo new site quickstart

# Copiamos o conteúdo de quickstart para a home do nosso repositório
mv quickstart/* . 

# E podemos deletar a pasta quickstart
rm -rf quickstart
```

4. Adicionamos um tema, no quickstart é utilizado o Ananke:

```shell
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke

echo theme = \"ananke\" >> config.toml
```

5. Para fazermos posts neste site usamos o Hugo também!
```shell
hugo new posts/my-first-post.md
```
    OBS: Após terminar de editá-lo, mude `draft` para `false`.
    
6. E por fim, para rodar o servidor do Hugo localmente para vermos o site usamos o seguinte comando:
```shell
hugo server -D
```

7. Agora só acessar `localhost:1313` que você verá o seu site!

![Ananke](ananke.png "Ananke Theme")

## GitHub Actions
Agora que temos um site completo falta uma última coisa, a integração com o GitHub Actions.
Isso vai servir para que o site atualize automaticamente toda vez que você fizer um push para o GitHub.

1. Crie um arquivo na pasta `.github` chamado `gh-pages.yml` com o seguinte conteúdo:
```yml
name: github pages

on:
  push:
    branches:
      - master # ou Main
  pull_request:

jobs:
  deploy:
    runs-on: ubuntu-20.04
    steps:
      - uses: actions/checkout@v2
        with:
          submodules: true 
          fetch-depth: 0

      - name: Setup Hugo
        uses: peaceiris/actions-hugo@v2
        with:
          hugo-version: 'latest'
          extended: true

      - name: Build
        run: hugo --minify

      - name: Deploy
        uses: peaceiris/actions-gh-pages@v3
        if: github.ref == 'refs/heads/master' # ou main
        with:
          github_token: ${{ secrets.GITHUB_TOKEN }}
          publish_dir: ./public
```

2. Altere qual branch será usada para o site nas configurações:

   Pelo fato da master ter o 'source code', o deploy é feito automaticamente na branch `gh-pages`.

   Para trocar, vá em Configurações > Pages.
   ![Configurações > Pages](settings.png "Settings")

3. Agora só dar push em tudo e visitar seu site!

## E fim...
Bom, espero não ter esquecido nada, mas de qualquer forma tomara que este texto tenha sido útil de alguma forma. Inté.

